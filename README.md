# git-v2

## Desctription

Repo de la formation git v2 : concepts avancés (branche, résolution de conflits...). **En cas de questions :** [thibaud.duhautbout@etu.utc.fr](mailto://thibaud.duhautbout@etu.utc.fr) ou [remy.huet@etu.utc.fr](mailto://remy.huet@etu.utc.fr).

Pour aller plus loin : lire la [documentation de git](https://git-scm.com/docs).

## Présentation

La présentation au format PDF est disponible [ici](https://gitlab.utc.fr/picasoft/formations/A18/git-v2/-/jobs/artifacts/master/raw/presentation.pdf?job=beamer-build)

